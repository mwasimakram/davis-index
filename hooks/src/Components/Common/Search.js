import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

export default function Search(props) {
    function onChangeString(event){
        props?.onChangeInput(event.target.value)
    }
    return (
        <div>
            <InputGroup className="mb-3">
                <InputGroup.Text ></InputGroup.Text>
                <Form.Control
                    placeholder="Search"
                    aria-label="Search"
                    onChange={onChangeString}
                />
            </InputGroup>
        </div>
    )
}