import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import './RightSideMenu.css';
import {SizeOpen,SizeClose} from '../../Utils/Helper';
import editing from "../../images/editing.jpeg"
import menu from "../../images/menu.jpeg"
import email from "../../images/email.jpeg"
const RightSideMenu = () => {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-3 right" id="rightt">
          <div className="bg-light row">
            {/* <h2>Sidebar Content</h2> */}
            {/* Sidebar content goes here */}
            <div className="row">
            <div className="col-sm-3 icons" id="icon">
                <div className="border-bottomm" id="bottom">
                <img src={editing} width={40} id="edit" className="menu-img-bg-light"/>
                <img src={menu} width={40} onClick={SizeOpen} id="mobile-menu" className="toggle"/>
                <img src={require('../../images/close.jpeg')} width={40} onClick={SizeClose} id="close" className="toggle"/>
                </div>
                <div className="border-bottomm" id="bottom">
                <img src={email} width={40} id="email" className="menu-img"/>
                <img src={require('../../images/sedan.jpeg')} width={40} id="sedan" className="menu-img"/>
                </div>
                <div className="border-bottomm" id="bottom">
                <img src={require('../../images/blank-page.jpeg')} width={40} id="blank" className="menu-img"/>
                <img src={require('../../images/user.jpeg')} width={40} id="user" className="menu-img"/>
                </div>
                <img src={require('../../images/information.jpeg')} width={40} id="information" className="menu-img"/>
            </div>
            <div className="col-sm-9 dynamic" id="dyna">
                <div className="outer outer1">
                    <h2 className="outer-heading">HUB</h2>
                    <div className="inner inner1">
                        <div className="data-heading">
                        <h4 className="inner-heading">STATUS</h4>
                        </div>
                        <div className="data">
                        <ul>
                            <li>In-progress</li>
                            <li>Completed</li>
                            <li>Withdrawn</li>
                            <li>Declined</li>
                            <li>Cancelled</li>
                        </ul>
                        </div>
                    </div>
                    <div className="inner inner2">
                        <div className="data-heading">
                        <h4 className="inner-heading">TIME</h4>
                        </div>
                        <div className="data">
                        <ul>
                            <li>In-progress</li>
                            <li>Completed</li>
                            <li>Withdrawn</li>
                            <li>Declined</li>
                            <li>Cancelled</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div className="outer outer2">
                    <h2 className="outer-heading">PRICE</h2>
                    <div className="inner inner1">
                        <h4 className="inner-heading">MATERIAL TYPE</h4>
                        <div className="data">
                        <ul>
                            <li>In-progress</li>
                            <li>Completed</li>
                            <li>Withdrawn</li>
                            <li>Declined</li>
                            <li>Cancelled</li>
                        </ul>
                        </div>
                    </div>
                </div>                
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );  
};

export default RightSideMenu;