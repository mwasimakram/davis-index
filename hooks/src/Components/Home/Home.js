import RightSideMenu from "../Common/RightSideMenu";
import Product from "../Products/Products";
import TopHeader from "../Common/TopHeader";
export default function Home(){
    return(<>
       <TopHeader></TopHeader>
       <div className="row">
            <div className="col-md-9">
                <Product></Product>
            </div>
            <div className="col-md-3">
              <RightSideMenu></RightSideMenu>
            </div>

        </div> 
    </>)
}