import { useEffect, useState } from "react";
import { CATEGORY_LIST } from "../../Utils/Constant";
import { getReq } from "../../Services/axios";

export default function CategoryList() {
    const [categoryList, setCategoryList] = useState([]);
    useEffect(() => {
        const getCategoryList = async () => {
            const response = await getReq(CATEGORY_LIST);
            console.log(response)
            setCategoryList(response.data);
        }
        getCategoryList();
    }, []);
    return (
        <>
             {categoryList && categoryList.length > 0 ? (
            categoryList.map((c) => {
                return <div className="mb-3">
                    <h3>{c}</h3>
                </div>
            })
        ) : (
            <p>Loading...</p>
        )}
        </>
    )
}