import { PRODUCT_LIST } from "../../Utils/Constant";
import { getReq } from "../../Services/axios";
import { useState, useEffect } from "react";
import Search from "../Common/Search";
import { useDispatch ,useSelector} from "react-redux";
import { setInitialProducts , searchProducts ,getInitialProducts} from "../../Action/ProductsAction";

export default function Product() {
    const dispatch = useDispatch();
    console.log(useSelector(state => state))
    const products = useSelector(state => state.product.searchResult)
    useEffect(() => {
        const getProductList = async () => {
            const response = await getReq(PRODUCT_LIST);
            console.log(response.data)
            dispatch(setInitialProducts(response.data));
        }
        getProductList();
    }, []);
    function getSearchString(query) {
        if(!query){
            dispatch(getInitialProducts(""))
            console.log(dispatch(getInitialProducts("")))
        }else{
            dispatch(searchProducts(query))
        }
    }
    return (<>
        <Search onChangeInput={getSearchString}></Search>
        {products && products.length > 0 ? (
            products.map((p) => {
                return <div>
                    <h3>{p.title}</h3>
                </div>
            })
        ) : (
            <p>Loading...</p>
        )}
    </>)
}