import axios from 'axios';
import {SERVER_URL} from '../Utils/Constant'
// axios.interceptors.request.use(async (request) => {
//     const token = localStorage.getItem('token');
//     if (!request.url.includes("/adminLogin")) {
//         request.headers.Authorization = `Bearer ${token}`;
//     }
//     return request;
// });

export async function getReq(url, config = {}) {
    try {
        const config = {
            method: "get",
            url: `${SERVER_URL + url}`,
        };

        let result = await axios(config);
        return result
    } catch (error) {
        return error;
    }
}
export async function postReq(url, body = {}, config = {}) {
    try {
        const config = {
            method: "post",
            url: `${SERVER_URL + url}`,
            data: body
        };

        let result = await axios(config);
        return result;
    } catch (error) {
        return error;
    }
}
export async function updateReq(url, body = {}, config = {}) {
    try {
        const config = {
            method: "put",
            url: `${SERVER_URL + url}`,
            data: body
        };
        let result = await axios(config);
        return result;
    } catch (error) {
        return error;
    }
}
export async function deleteReq(url, id, config = {}) {
    try {
        const config = {
            method: "delete",
            url: `${SERVER_URL + url + "/" + id}`,
        };
        let result = await axios(config);
        return result;
    } catch (error) {
        return error;
    }
}

async function checkTokenExpiry() {
    try {
        var isExpired = false;
        const token = localStorage.getItem('token');
        var decodedToken = await JSON.parse(atob(token.split('.')[1]));
        console.log("token decode",decodedToken)
        var dateNow = new Date();

        if (decodedToken.exp * 1000 < dateNow.getTime()){
            isExpired = true;
        }
        return isExpired;
        
    } catch (error) {
        console.log(error);
        return;
    }
}