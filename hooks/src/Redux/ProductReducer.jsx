import { SET_INITIAL_PRODUCTS,SEARCH_PRODUCTS,GET_INITIAL_PRODUCTS } from "../Utils/Constant"
const initialState = {
    searchResult: []
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_INITIAL_PRODUCTS:
            initialState.searchResult = action.payload;
            return { ...state, searchResult: action.payload };

        case SEARCH_PRODUCTS:
            let filterArray = state.searchResult.filter((item) =>
                item.title.toLowerCase().includes(action.payload.toLowerCase())
            );
            return {...state,searchResult:filterArray}
        case GET_INITIAL_PRODUCTS:
            return {...state,searchResult: initialState.searchResult }
        default:
            return state;
    }
};

export default productReducer;