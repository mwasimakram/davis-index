export function SizeOpen(){
    var sidebar = document.getElementById('rightt');
    var dynamic = document.getElementById('dyna');
    var mobileMenu = document.getElementById('mobile-menu');
    var close = document.getElementById('close');
    var iconsborder = document.getElementById('icon');
    var borderbotom = document.getElementById('bottom');

    sidebar.style.width = "25%";
    iconsborder.style.borderRight = "1px solid #dddddd";
    dynamic.style.display = "inline-block";
    mobileMenu.style.display = "none";
    close.style.display = "inline-block";
    borderbotom.style.borderBottom = "1px solid #dddddd";
  }

export function SizeClose(){
    var sidebar = document.getElementById('rightt');
    var dynamic = document.getElementById('dyna');
    var mobileMenu = document.getElementById('mobile-menu');
    var close = document.getElementById('close');
    var iconsborder = document.getElementById('icon');
    var borderbotom = document.getElementById('bottom');

    sidebar.style.width = "6%";
    iconsborder.style.borderRight = "none";
    dynamic.style.display = "none";
    mobileMenu.style.display = "inline-block";
    close.style.display = "none";
    borderbotom.style.borderBottom = "1px solid #dddddd";
  }