export const SERVER_URL="https://fakestoreapi.com";
export const CATEGORY_LIST="/products/categories";
export const PRODUCT_LIST="/products"
export const SET_INITIAL_PRODUCTS = 'SET_INITIAL_PRODUCTS';
export const SEARCH_PRODUCTS = 'SEARCH_PRODUCTS';
export const GET_INITIAL_PRODUCTS = 'GET_INITIAL_PRODUCTS';