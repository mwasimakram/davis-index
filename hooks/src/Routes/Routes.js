import { Routes,Route } from 'react-router-dom';
import AuthenticatedRoute  from './AuthencitaedRoutes.';
import SignIn from "../Components/User/SignIn";
import Home from "../Components/Home/Home";

export default function RoutesList() {
    return (
        <Routes>
            <Route  path="/" element={<SignIn />} />
            <Route  path="/home" element={<Home />} />
            <Route path="/private" element={<AuthenticatedRoute />} />
        </Routes>
    )
}